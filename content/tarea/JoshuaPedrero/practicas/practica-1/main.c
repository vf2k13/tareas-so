/*
* ServerChikito
* Copyright (C) 2020  uwuOS Enterprises
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
// -- Self headers --
#include "structs.h"
#include "server.h"

int main(int argc, char const *argv[]) {

  struct sock server = {0}; // Inicializa la estructura con 0's
  FillWithArgs(argc, argv, &server);
  StartServer(&server);

  return 0;

} 
