// ServerChikito
// Copyright (C) 2020  uwuOS Enterprises
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include <stdlib.h>
// -- Self headers--
#include "structs.h"
#include "errors.h"

#define BUFF_SIZE 1024

pid_t childpid; //PID del hijo
char buffer[BUFF_SIZE];

extern char *optarg;
extern int optind;

void FillWithArgs(int argc, char * const argv[], struct sock *cfg) {}

  /*char address[16];       // direccion IP*/
  /*unsigned int port;      // puerto*/
  /*char path[30];          // root path*/
  /*unsigned int n_process; // numero de procesos a lanzar*/
  /*int opt;*/

  /*// Parsing de los argumentos*/
  /*while ((opt = getopt(argc, argv, ":a:p:r:t:")) != -1) {*/
    /*switch (opt) {*/
    /*case 'a': // Parsing de la direccion IP*/
      /*strcpy(address, optarg); // Posible bug cuando optarg sea mayor a address.*/
      /*break;*/
    /*case 'p': // Parsing del puerto de escucha*/
      /*port = ValidPort(atoi(optarg));*/
      /*break;*/
    /*case 'r': // Parsing del root path*/
      /*strcpy(path, optarg); //Falta comprobar que sea valida.*/
      /*break;*/
    /*case 't': // Parsing del número de procesos*/
      /*n_process = ValidNProc(atoi(optarg));*/
      /*break;*/
    /*case ':': // Parámetros incompletos*/
      /*printf("Hace falta el valor de un parametro\n");*/
      /*exit(EXIT_FAILURE);*/
    /*case '?':*/
      /*printf("No se reconoce el argumento dado\n");*/
      /*exit(EXIT_FAILURE);*/
    /*}*/
  /*}*/

  /*// No hay argumentos entonces ponemos valores por default.*/
  /*if (optind < 2) {*/
    /*strcpy(address, "127.0.0.1");*/
    /*port = 8080;*/
    /*strcpy(path, "/");*/
    /*n_process = 4;*/
  /*}*/

  /*cfg->addr.sin_family = AF_INET; // Asignamos la familia*/
  /*cfg->addr.sin_port = htons(port); // Asignamos puerto*/
  /*AddressFormat(inet_aton(address, &cfg->addr.sin_addr)); // Asignamos ip*/
  /*strcpy(cfg->path, path); // Asignamos path*/
  /*cfg->n_process = n_process; // Asignamos numero de procesos*/

/*}*/

int CreateServer(struct sock *server) { return 0; }

  /*server->fd = socket(AF_INET, SOCK_STREAM, 0);*/

  /*// Manejo de error en la creación del socket*/
  /*if (server->fd == -1) {*/
    /*printf("Error abriendo socket\n");*/
    /*return 1;*/
  /*}*/

  /*// configuramos el socket*/
  /*int optval = 1;*/
  /*if (setsockopt(server->fd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval)) == -1) {*/
    /*fprintf(stderr, "Error al configurar el socket");*/
    /*// limpiando*/
    /*shutdown(server->fd, SHUT_WR);*/
    /*close(server->fd);*/
    /*exit(EXIT_FAILURE);*/
  /*}*/

  /*// solicitamos al sistema operativo la direccion y puerto*/
  /*if (bind(server->fd, (struct sockaddr *)&server->addr, sizeof(server->addr)) == -1) {*/
    /*printf("Error asociando a la direccion %s puerto %d\n", inet_ntoa(server->addr.sin_addr), ntohs(server->addr.sin_port));*/
    /*// limpiando*/
    /*close(server->fd);*/
    /*return 3;*/
  /*}*/

  /*// escuchamos por nuevas conexiones*/
  /*if (listen(server->fd, 10) == -1) {*/
    /*printf("Error al escuchar\n");*/
    /*// limpiando*/
    /*close(server->fd);*/
    /*return 4;*/
  /*}*/

  /*return 0;*/
/*}*/

/*int ServeClient(struct sock *server, struct sock *client) {*/

  /*close(server->fd); //Ya no usaremos el file descriptor del padre.*/

  /*client->fp = fdopen(client->fd, "r+");*/

  /*while (1) {*/
    /*// TODO(aldoherzac): peticion GET*/
    /*// TODO(victor): peticion POST*/
    /*// TODO(fernando): peticion DELETE*/
    /*// max_request -= 1;*/
    /*recv(client->fd, buffer, BUFF_SIZE, 0);*/

    /*if (client->fp == NULL) {*/
      /*fputs("Error abriendo socket como stream", stderr);*/
      /*// Limpiando*/
      /*shutdown(client->fd, SHUT_WR);*/
      /*close(client->fd);*/
    /*}*/
    /*//resolve(client->fp, &buffer);*/
  /*}*/

  /*return 0;*/
/*}*/

void StartServer(struct sock *server) {}

  /*CreateServer(server);*/
  /*//Socket para el cliente*/
  /*struct sock client = {0}; // Inicializa la estructura con 0's*/
  /*int max_process = server->n_process;*/

  /*char buf[BUFF_SIZE]; // Tamanio del buffer para guardar la petición del cliente*/
  
  /*// Dado el numero de procesos especificado (4 por default)*/
  /*// se crea un proceso para cada uno*/
  /*for(int i = 0; i<max_process; i++) {*/
    /*int childpid = fork();*/
    /*if (childpid == 0) { // Si es el proceso hijo ...*/
      /*while(1) {*/
        /*client.fd = accept(server->fd, (struct sockaddr *)&client.addr, &client.addr.sin_addr.s_addr);*/
        /*printf("Se establecio la conexion con %s en el puerto %d\n", inet_ntoa(client.addr.sin_addr), ntohs(client.addr.sin_port));*/
        /*printf("Atendido por el proceso: %d\n", getppid());*/
        /*printf("Peticion:\n");*/
        /*int n = read(client.fd, buf, sizeof(buf));*/
        /*write(STDOUT_FILENO, buf, n);*/
        /*close(client.fd);*/
      /*}*/
    /*} else if(childpid > 0) { // Si es el proceso padre ---*/
      /*printf("ID del proceso: %d\n", childpid);*/
    /*}*/
  /*}*/

  /*close(client.fd); //Ya no usaremos el file descriptor del cliente.*/
//}
