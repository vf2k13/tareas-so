#include <ctype.h> // para usar tolower
#include <string.h> // para user strrchr

// Este es el programa para manejar los tipos mimes.
// Función que devuelve el tipo de un archivo

typedef struct {
  const char *extension;
  const char *mime_type;
} mime_map;

//#define NULL ((char *)0)
#define DEFAULT_MIME_TYPE "application/octet-stream"

// MIME types permitidos
mime_map meme_types [] = {
  {".css", "text/css"},
  {".gif", "image/gif"},
  {".htm", "text/html"},
  {".html", "text/html"},
  {".jpeg", "image/jpeg"},
  {".jpg", "image/jpeg"},
  {".ico", "image/x-icon"},
  {".js", "application/javascript"},
  {".pdf", "application/pdf"},
  {".mp4", "video/mp4"},
  {".png", "image/png"},
  {".svg", "image/svg+xml"},
  {".xml", "text/xml"},
  {NULL, NULL},
};

char *default_mime_type = "text/plain";


char *minuscula(char *s) {

  for (char *p = s; *p != '\0'; p++) {
    *p = tolower(*p);
  }

  return s;
}
 
char *obtenerMime(char *filename) {

  // Obtenemos la extension del archivo
  char *ext = strrchr(filename, '.');

  // Si no se encuentra una extención válida mandamos el tipo por defecto
  if (ext == NULL) {
    return "text/plain";
  }

  ext++;

  // Convertimos el nombre de la extension a miniusculas
  minuscula(ext);

  // Regresamos el tipo mime según sea el caso.
  if (strcmp(ext, "html") == 0 || strcmp(ext, "htm") == 0) { return "text/html"; }
  if (strcmp(ext, "jpeg") == 0 || strcmp(ext, "jpg") == 0) { return "image/jpg"; }
  if (strcmp(ext, "css") == 0) { return "text/css"; }
  if (strcmp(ext, "js") == 0) { return "application/javascript"; }
  if (strcmp(ext, "json") == 0) { return "application/json"; }
  if (strcmp(ext, "txt") == 0) { return "text/plain"; }
  if (strcmp(ext, "gif") == 0) { return "image/gif"; }
  if (strcmp(ext, "png") == 0) { return "image/png"; }

  return "text/plain";

}
