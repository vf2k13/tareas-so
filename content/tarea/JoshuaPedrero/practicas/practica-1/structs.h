#ifndef TYPES_H
#define TYPES_H

#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>

struct sock {
    int fd;
    FILE *fp;
    char path[30];
    int n_process;
    struct sockaddr_in *addr; // Dir y Puerto
};

typedef struct {
    char nombreArchivo[512];
    off_t offset;              
    size_t end;
} peticionHttp;

//typedef struct {
//    const char *extension;
//    const char *mime_type;
//} mime_map;
//
//char *default_mime_type = "text/plain";
//
//mime_map meme_types [] = {
//  {".css", "text/css"},
//  {".gif", "image/gif"},
//  {".htm", "text/html"},
//  {".html", "text/html"},
//  {".jpeg", "image/jpeg"},
//  {".jpg", "image/jpeg"},
//  {".ico", "image/x-icon"},
//  {".js", "application/javascript"},
//  {".pdf", "application/pdf"},
//  {".mp4", "video/mp4"},
//  {".png", "image/png"},
//  {".svg", "image/svg+xml"},
//  {".xml", "text/xml"},
//  {NULL, NULL},
//};

#endif
