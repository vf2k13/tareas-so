#include <arpa/inet.h>          /* inet_ntoa */
#include <signal.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <time.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/sendfile.h> //probar en linux si hay error en esta linea
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
// -- Self headers --
#include "structs.h"
#include "errors.h"
#include "mime.h"

#define MAXLINE 1024   /* max length of a line */

// Función atiendePeticionDirectorio
// int out_fd el descriptor del  socket 
// int dir_fd el descriptor del directrorio 
// char* nombreArchivo Nombre del archivo
void atiendePeticionDirectorio(int out_fd, int dir_fd, char *nombreArchivo){

  // Definimos buffers para poner la fecha etc.
  char buf[MAXLINE], m_time[32], size[16];
  struct stat statbuf;

  // Imprimimos la respuesta con un formsto adecuado
  sprintf(buf, "HTTP/1.1 200 OK\r\n%s%s%s%s%s",
               "Content-Type: text/html\r\n\r\n",
               "<html><head><style>",
               "body{font-family: monospace; font-size: 13px;}",
               "td {padding: 1.5px 6px;}",
               "</style></head><body><table>\n");

  // writen(out_fd, buf, strlen(buf));
  // Escribribimos en el buffer.
  write(out_fd, buf, strlen(buf));

  // Abrimos un stream de directorio y lo asociamos a un puntero
  DIR *d = fdopendir(dir_fd);

  struct dirent *dp; // 

  int ffd;

  // Leemos archivo por archivo hasta que sea null
  while ((dp = readdir(d)) != NULL) { // Leemos el directorio

    if (!strcmp(dp->d_name, ".") || !strcmp(dp->d_name, "..")) { // Si todavia hay mas por buscar busca.
      continue;
    }

    // Abrimos el archvo y si obtenemos un error mostramos el error.
    if ((ffd = openat(dir_fd, dp->d_name, O_RDONLY)) == -1) {
      perror(dp->d_name); // Mandamos un error cuando queremos leer un archivo de solo lectura
      continue;
    }

    // Rellenamos a estructura statbuf con el ffd.
    fstat(ffd, &statbuf);

    // Formateamos na fecha que tenemos que pasar como parametro
    strftime(m_time, sizeof(m_time),
             "%Y-%m-%d %H:%M", localtime(&statbuf.st_mtime));
    // format_size(size, &statbuf);

    // Si el archivo es regular o es directorio
    if (S_ISREG(statbuf.st_mode) || S_ISDIR(statbuf.st_mode)) {
      // Vemos si tenemos permisos para descender en directorios. 
      char *d = S_ISDIR(statbuf.st_mode) ? "/" : "";

      // Imprimimos la respueta.
      sprintf(buf, "<tr><td><a href=\"%s%s\">%s%s</a></td><td>%s</td><td>%s</td></tr>\n",
              dp->d_name, d, dp->d_name, d, m_time, size);
      // Escribimos en el archivo en el que apunta el descriptor de archivo
      write(out_fd, buf, strlen(buf));
    }

    // Cerramos el descriptor de archivo
    close(ffd);
  }

  // Guardamos en el buffer
  sprintf(buf, "</table></body></html>");

  // Escribimos en el archivo en el que apunta el descriptor de archivo
  write(out_fd, buf, strlen(buf));
  closedir(d); // Cerramos 
}

// Funcion que atiende la peticion en el caso de un archivo.
// int out_fd descriptor de archivo del socket cliente
// int in_fd descriptor de archivo del archivo a servir 
// total_size Es el tamaño en toal del archivo 
void atiendePeticion(int out_fd, int in_fd, peticionHttp *req, size_t total_size) {
  char buf[256]; // Definimos un buffer para la respuesta.

  if (req->offset > 0){ // Si te pasaste del limite de memoria 
    // Imprimimos la respuesta
    sprintf(buf, "HTTP/1.1 206 Partial\r\n");
    sprintf(buf + strlen(buf), "Content-Range: bytes %lu-%lu/%lu\r\n",
            req->offset, req->end, total_size);
  } else {
    // Estamos en el rango aceptado
    sprintf(buf, "HTTP/1.1 200 OK\r\nAccept-Ranges: bytes\r\n");
  }
  //sprintf(buf + strlen(buf), "Cache-Control: no-cache\r\n");

  // Obtenemos e imprimimos la longitud del contenido
  sprintf(buf + strlen(buf), "Content-length: %lu\r\n",
          req->end - req->offset);

  //Obtenemos el tipo mime del archivo
  sprintf(buf + strlen(buf), "Content-type: %s\r\n\r\n",
          obtenerMime(req->nombreArchivo)); // TODO implementar mime_type

  write(out_fd, buf, strlen(buf)); // TODO manejar el error
  off_t offset = req->offset; 
  while (offset < req->end) {
    // Transferencia de archivo entre out_fd e in_fd
    if(sendfile(out_fd, in_fd, &offset, req->end - req->offset) <= 0) {
      break;
    }
    // Imprimimos el offset
    // printf("offset: %d \n\n", offset);
    close(out_fd); // Cerramos el descriptor de archivo del socket
    break;
  }
}

// Metodo que se encarga de enviar una respuesta a partir de una solicitud GET.
// Tipo del archivo
int SendGetResponse(int sockfd, char *tipo) {

  // El mensaje no tiene que ser vacío

  // Calculamos el tamaño del mensaje
  char *mensaje;
  int tamanio_mensaje;
      tamanio_mensaje=strlen(mensaje);
      tamanio_mensaje+=strlen("%s %s%s%s HTTP/1.0\r\n");

  // Tenemo que obtener el tipo mime del archivo
  // tamanio_mensaje+=strlen("%s %s%s%s Content-Type: "+tipo+"; charset=utf-8\r\n");
  // tamanio_mensaje+=strlen("Content-Type: %s ; charset=utf-8\r\n", tipo);

  tamanio_mensaje+=strlen("\r\n");

  // Reservamos espacio de memoria
  mensaje = malloc(tamanio_mensaje);

  // Imprimimos lo que tenemos que enviar.
  printf("peticion:\n%s\n",mensaje);

  // Enviamos la petición

  int total = strlen(mensaje);
  int enviado,bytes = 0;

  // Escribimos byte a byte para enviar la petición
  for(int enviado=0; enviado < total; enviado++) {
     
    // Escibimos el mensaje a enviar en memoria
    bytes = write(sockfd,mensaje+enviado,total-enviado);
  }
}

// Funcion que se encarga de servir al cliente
// fd es el file descriptor del socket del cliente
void ServeClient(int fd, struct sock *server, struct sock *client) {
  
  printf("Peticion aceptada el pid is %d\n", getpid()); // Obtenemos el pid del hijos

  peticionHttp peticion; // Definimos la estructura de la peticion http.
  struct stat sbuf; // Bufer

  // Abrimos el arhivo
  int status = 200, ffd = open(peticion.nombreArchivo, O_RDONLY, 0);
  
  // El ffd es el file descriptor del archivo solicitado 
  if (ffd <= 0) {
    status = 404; // Archivo no encontrado
    char *descripcion = "Archivo no encontrado";
    // Mandamos a la función error cliente
    errorCliente(fd,status, "No encontrado", descripcion);
  } else {
    fstat(ffd, &sbuf); // Se rellena la informa de la estructura stat con la informacion file del descriptor del archivo 
    // Si el archivo es regular
    if(S_ISREG(sbuf.st_mode)){ // Vemos los permisos del archivo solicitado con st_mode
      if (peticion.end == 0) { 
        peticion.end = sbuf.st_size; // Vemos el tamaño total en bytes.  
      }
      if (peticion.offset > 0) {
        status = 206; // Exito pero el cuerpo contiene los rangos solicitados de informacion
      }
      atiendePeticion(fd, ffd, &peticion, sbuf.st_size);
    } else if (S_ISDIR(sbuf.st_mode)) { // Si el archivo es un directorio
      status = 200;
      atiendePeticionDirectorio(fd, ffd, peticion.nombreArchivo);
    } else {
      status = 400; // Error 400 http
      char *descripcion = "Error desconocido";
      // Llamamos a la función client_error para imprimir el error http.
      errorCliente(fd, status, "Error", descripcion);
    }
    
    // Cerramos el descriptor de archivo del archivo
    close(ffd); 
  }

} // END ServeClient
