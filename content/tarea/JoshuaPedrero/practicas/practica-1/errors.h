#ifndef ERROR_H
#define ERROR_H

// Verificar que la direccion tiene el formato: n.n.n.n
void AddressFormat(int addres);

// Verifica si el puerto es valido.
int ValidPort(int port);

// Varifica si el numero de procesos es valido.
int ValidNProc(int n_process);

// Checa que las conexiones no superen el maximo indicado.
void ExededConnections(int max_process);

void errorCliente(int fd, int status,char *mensaje, char *descripcion);

// Indica errores en las peticiones
void BadRequest(int code, char* message);

// Indica cuando existe un intento de subir de directorio
void ScaleDirectoryWarning(char* message);

// Función que valida que la informacion ingresada sea valida, 
// recibimos la expresion regex como parametro 
int ValidaInformacion(char string[87], char *address);

#endif
