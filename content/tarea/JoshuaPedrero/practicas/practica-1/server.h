#ifndef SERVER_H
#define SERVER_H

#include "structs.h"

// Rellena los campos de la estructura sock con los parametros pasados como
// argumento
void FillWithArgs(int argc, char const *argv[], struct sock *cfg);

// Crea un File Descriptor  utilizando la informacion de la structura sock
void CreateServer(struct sock *server);

// Inicia la escucha del socket
// Acepta tantas peticiones de clientes como número de procesos fueron
// especificados
// Crea un file descriptor del socket creado al aceptar cada petición del cliente
void StartServer(struct sock *server);

// Se encarga de servir las peticiones del cliente dependiendo de su naturaleza
void ServeClient(struct sock *server, struct sock *client);

int SendGetResponse(char *cadena);
// void sendRequest()
// void receiveRequest()

#endif //SERVER_H
