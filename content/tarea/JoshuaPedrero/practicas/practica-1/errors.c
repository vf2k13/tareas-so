// ServerChikito
// Copyright (C) 2020  uwuOS Enterprises
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// Importamos la librería para expresiones regulares.
#include <regex.h>

#define MAXLINE 1024

char funcionamiento[80] = "USO: -a n.n.n.n -p m -r root/path -t j\n n de n de 0-255\n m de 1-65535\n t de 1-80";

void AddressFormat(int address) {
  if (address == 0) {
    printf("%s\n Error en el formato de IPv4: n.n.n.n con n de 0-255\n", funcionamiento);
    exit(EXIT_FAILURE);
  }
}

int ValidPort(int port) {
  if (port <= 0 || port > 65535) {
      printf("%s\n Error en el valor del puerto: 1 - 65535\n", funcionamiento);
      exit(EXIT_FAILURE);
  }
  return port;
}

// Este método que encarga de manejar cuando hay un error
void errorCliente(int fd, int status,char *mensaje, char *descripcion) {

  // Imprimimos el mensaje de error
  char buf[MAXLINE];
  sprintf(buf, "HTTP/1.1 %d %s\r\n", status, mensaje);

  sprintf(buf + strlen(buf),
          "Content-length: %lu\r\n\r\n", strlen(descripcion));

  sprintf(buf + strlen(buf), "%s", descripcion);
}

int ValidNProc(int n_process) {
  if (n_process == 0 || n_process > 80) {
    printf("%s\n Error el valor de procesos es entre 1 y 80\n", funcionamiento);
    exit(EXIT_FAILURE);
  }
  return n_process;
}

void ExededConnections(int max_process) {
  if (max_process < 0) {
    printf("El numero de procesos llego a su maximo\n");
    // TODO(hemora96): Impedir nuevas conexiones mas no cerrar las conexiones abiertas actuales
    exit(EXIT_FAILURE);
  }
  printf("-- Quedan %d conexiones disponibles. --\n", max_process);
}

int ValidaInformacion(char *expressionRegex, char *cadenaComparada) {
 
  // Variable para guardar el regex.
  regex_t regex;

  // Variable que retorna el tipo.
  int value;

  // Creación de la estructura para  regex
  value = regcomp(&regex, expressionRegex, 0);

  // Comparamos cadenaComparada con el regex definido
  value = regexec(&regex, cadenaComparada,0, NULL, 0);

  // El patron es correctoo
  if (value == 0) {
    printf("Dato valido.\n");
    return 0;
  }
  // El patron es incorrecto
  else if (value == REG_NOMATCH) {
    printf("Dato no valido.\n");
    return -1;
  }
  else {
    printf("Error.\n");
    return -2;
  }
}

/*int ValidIPv4(char *address) {*/
  /*// Regex para direcciones ipv4 validas*/
  /*int resultado;*/
  /*resultado = ValidaInformacion("'\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}\b'",address);*/

  /*// Regresamos el valor*/
  /*return resultado;*/
/*}*/

